/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package accessmysql.model;

import java.io.Serializable;

/**
 *
 * @author JcMoutoh
 */
public class EbuPackage  implements Serializable{
    
  private int packageId;
  private String packageName;
  private String segmentId;
  private int packagePrice ;
  private int voiceAmout ;
  private String refillVoice;
  private String voiceOfferTxt;
  private int smsAmount;
  private String refillSms;
  private int dataAmount;
  private String refillData ;
  private String packageKeyword;
  private String packageConfirmationTxt ;
  private String packageActive ;
  private int pacakgeOrder ;
  private String duration ;

    /**
     * @return the packageId
     */
    public int getPackageId() {
        return packageId;
    }

    /**
     * @param packageId the packageId to set
     */
    public void setPackageId(int packageId) {
        this.packageId = packageId;
    }

    /**
     * @return the packageName
     */
    public String getPackageName() {
        return packageName;
    }

    /**
     * @param packageName the packageName to set
     */
    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    /**
     * @return the segmentId
     */
    public String getSegmentId() {
        return segmentId;
    }

    /**
     * @param segmentId the segmentId to set
     */
    public void setSegmentId(String segmentId) {
        this.segmentId = segmentId;
    }

    /**
     * @return the packagePrice
     */
    public int getPackagePrice() {
        return packagePrice;
    }

    /**
     * @param packagePrice the packagePrice to set
     */
    public void setPackagePrice(int packagePrice) {
        this.packagePrice = packagePrice;
    }

    /**
     * @return the voiceAmout
     */
    public int getVoiceAmout() {
        return voiceAmout;
    }

    /**
     * @param voiceAmout the voiceAmout to set
     */
    public void setVoiceAmout(int voiceAmout) {
        this.voiceAmout = voiceAmout;
    }

    /**
     * @return the refillVoice
     */
    public String getRefillVoice() {
        return refillVoice;
    }

    /**
     * @param refillVoice the refillVoice to set
     */
    public void setRefillVoice(String refillVoice) {
        this.refillVoice = refillVoice;
    }

    /**
     * @return the voiceOfferTxt
     */
    public String getVoiceOfferTxt() {
        return voiceOfferTxt;
    }

    /**
     * @param voiceOfferTxt the voiceOfferTxt to set
     */
    public void setVoiceOfferTxt(String voiceOfferTxt) {
        this.voiceOfferTxt = voiceOfferTxt;
    }

    /**
     * @return the smsAmount
     */
    public int getSmsAmount() {
        return smsAmount;
    }

    /**
     * @param smsAmount the smsAmount to set
     */
    public void setSmsAmount(int smsAmount) {
        this.smsAmount = smsAmount;
    }

    /**
     * @return the refillSms
     */
    public String getRefillSms() {
        return refillSms;
    }

    /**
     * @param refillSms the refillSms to set
     */
    public void setRefillSms(String refillSms) {
        this.refillSms = refillSms;
    }

    /**
     * @return the dataAmount
     */
    public int getDataAmount() {
        return dataAmount;
    }

    /**
     * @param dataAmount the dataAmount to set
     */
    public void setDataAmount(int dataAmount) {
        this.dataAmount = dataAmount;
    }

    /**
     * @return the refillData
     */
    public String getRefillData() {
        return refillData;
    }

    /**
     * @param refillData the refillData to set
     */
    public void setRefillData(String refillData) {
        this.refillData = refillData;
    }

    /**
     * @return the packageKeyword
     */
    public String getPackageKeyword() {
        return packageKeyword;
    }

    /**
     * @param packageKeyword the packageKeyword to set
     */
    public void setPackageKeyword(String packageKeyword) {
        this.packageKeyword = packageKeyword;
    }

    /**
     * @return the packageConfirmationTxt
     */
    public String getPackageConfirmationTxt() {
        return packageConfirmationTxt;
    }

    /**
     * @param packageConfirmationTxt the packageConfirmationTxt to set
     */
    public void setPackageConfirmationTxt(String packageConfirmationTxt) {
        this.packageConfirmationTxt = packageConfirmationTxt;
    }

    /**
     * @return the packageActive
     */
    public String getPackageActive() {
        return packageActive;
    }

    /**
     * @param packageActive the packageActive to set
     */
    public void setPackageActive(String packageActive) {
        this.packageActive = packageActive;
    }

    /**
     * @return the pacakgeOrder
     */
    public int getPacakgeOrder() {
        return pacakgeOrder;
    }

    /**
     * @param pacakgeOrder the pacakgeOrder to set
     */
    public void setPacakgeOrder(int pacakgeOrder) {
        this.pacakgeOrder = pacakgeOrder;
    }

    /**
     * @return the duration
     */
    public String getDuration() {
        return duration;
    }

    /**
     * @param duration the duration to set
     */
    public void setDuration(String duration) {
        this.duration = duration;
    }

   
}
