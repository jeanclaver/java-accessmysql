/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package accessmysql.model;

/**
 *
 * @author jcmoutoh
 */
public class Subscriber {
    private int msisdn;
    private int serviceClass;
    private double balanceBefore;
    private double balanceAfter;

    /**
     * @return the msisdn
     */
    public int getMsisdn() {
        return msisdn;
    }

    /**
     * @param msisdn the msisdn to set
     */
    public void setMsisdn(int msisdn) {
        this.msisdn = msisdn;
    }

    /**
     * @return the serviceClass
     */
    public int getServiceClass() {
        return serviceClass;
    }

    /**
     * @param serviceClass the serviceClass to set
     */
    public void setServiceClass(int serviceClass) {
        this.serviceClass = serviceClass;
    }

    /**
     * @return the balanceBefore
     */
    public double getBalanceBefore() {
        return balanceBefore;
    }

    /**
     * @param balanceBefore the balanceBefore to set
     */
    public void setBalanceBefore(double balanceBefore) {
        this.balanceBefore = balanceBefore;
    }

    /**
     * @return the balanceAfter
     */
    public double getBalanceAfter() {
        return balanceAfter;
    }

    /**
     * @param balanceAfter the balanceAfter to set
     */
    public void setBalanceAfter(double balanceAfter) {
        this.balanceAfter = balanceAfter;
    }
    
}
