/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package accessmysql.model;
import java.io.Serializable;


/**
 *
 * @author JcMoutoh
 */
public class UserOption implements Serializable {
    private int price;
    private String optionEnum;
    private String optionText;
    private String confirmText;
    private int optionID;
    /**
     * @return the price
     */
    public int getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(int price) {
        this.price = price;
    }

    public String getOptionEnum() {
        return optionEnum;
    }

    /**
     * @return the option
     */
    public void setOptionEnum(String optionEnum) {    
        this.optionEnum = optionEnum;
    }

    /**
     * @return the optionText
     */
    public String getOptionText() {
        return optionText;
    }

    /**
     * @param optionText the optionText to set
     */
    public void setOptionText(String optionText) {
        this.optionText = optionText;
    }

    /**
     * @return the confirmText
     */
    public String getConfirmText() {
        return confirmText;
    }

    /**
     * @param confirmText the confirmText to set
     */
    public void setConfirmText(String confirmText) {
        this.confirmText = confirmText;
    }

    public int getOptionID() {
        return optionID;
    }

    public void setOptionID(int optionID) {
        this.optionID = optionID;
    }
    
}
